﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
[assembly: OwinStartup(typeof(SignalRApp.SignalR.SignalRStart))]
namespace SignalRApp.SignalR
{
    public class SignalRStart
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalHost.DependencyResolver.Register(
		typeof(ChatHub), 
		() => new ChatHub());
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR("/chat",new HubConfiguration
            {
                EnableJavaScriptProxies=true,
                EnableDetailedErrors=true
            });
        }
    }
}