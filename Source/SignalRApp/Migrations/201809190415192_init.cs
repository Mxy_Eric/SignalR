namespace SignalRApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SignalR_Chat_ChatContent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Picture_Id = c.Int(),
                        Voice_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SignalR_Chat_ChatPicture", t => t.Picture_Id)
                .ForeignKey("dbo.SignalR_Chat_ChatVoice", t => t.Voice_Id)
                .Index(t => t.Picture_Id)
                .Index(t => t.Voice_Id);
            
            CreateTable(
                "dbo.SignalR_Chat_ChatPicture",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        Suffix = c.String(),
                        CreateTime = c.String(),
                        UpdateTime = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SignalR_Chat_ChatVoice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        CreateTime = c.String(),
                        UpdateTime = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SignalR_Chat_ChatGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SignalR_User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NickName = c.String(maxLength: 15),
                        UserName = c.String(maxLength: 15),
                        PhoneNum = c.String(maxLength: 15),
                        Email = c.String(),
                        Password = c.String(maxLength: 20),
                        CreateTime = c.String(),
                        UpdateTime = c.String(),
                        HeadImg_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SignalR_UserHeadImg", t => t.HeadImg_Id)
                .Index(t => t.HeadImg_Id);
            
            CreateTable(
                "dbo.SignalR_UserHeadImg",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        Suffix = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SignalR_Chat_ChatRecord",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreateTime = c.String(),
                        UpdateTime = c.String(),
                        Content_Id = c.Int(),
                        From_Id = c.Int(),
                        ToGroup_Id = c.Int(),
                        ToUser_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SignalR_Chat_ChatContent", t => t.Content_Id)
                .ForeignKey("dbo.SignalR_User", t => t.From_Id)
                .ForeignKey("dbo.SignalR_Chat_ChatGroup", t => t.ToGroup_Id)
                .ForeignKey("dbo.SignalR_User", t => t.ToUser_Id)
                .Index(t => t.Content_Id)
                .Index(t => t.From_Id)
                .Index(t => t.ToGroup_Id)
                .Index(t => t.ToUser_Id);
            
            CreateTable(
                "dbo.UserChatGroups",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        ChatGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.ChatGroup_Id })
                .ForeignKey("dbo.SignalR_User", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.SignalR_Chat_ChatGroup", t => t.ChatGroup_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.ChatGroup_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SignalR_Chat_ChatRecord", "ToUser_Id", "dbo.SignalR_User");
            DropForeignKey("dbo.SignalR_Chat_ChatRecord", "ToGroup_Id", "dbo.SignalR_Chat_ChatGroup");
            DropForeignKey("dbo.SignalR_Chat_ChatRecord", "From_Id", "dbo.SignalR_User");
            DropForeignKey("dbo.SignalR_Chat_ChatRecord", "Content_Id", "dbo.SignalR_Chat_ChatContent");
            DropForeignKey("dbo.SignalR_User", "HeadImg_Id", "dbo.SignalR_UserHeadImg");
            DropForeignKey("dbo.UserChatGroups", "ChatGroup_Id", "dbo.SignalR_Chat_ChatGroup");
            DropForeignKey("dbo.UserChatGroups", "User_Id", "dbo.SignalR_User");
            DropForeignKey("dbo.SignalR_Chat_ChatContent", "Voice_Id", "dbo.SignalR_Chat_ChatVoice");
            DropForeignKey("dbo.SignalR_Chat_ChatContent", "Picture_Id", "dbo.SignalR_Chat_ChatPicture");
            DropIndex("dbo.UserChatGroups", new[] { "ChatGroup_Id" });
            DropIndex("dbo.UserChatGroups", new[] { "User_Id" });
            DropIndex("dbo.SignalR_Chat_ChatRecord", new[] { "ToUser_Id" });
            DropIndex("dbo.SignalR_Chat_ChatRecord", new[] { "ToGroup_Id" });
            DropIndex("dbo.SignalR_Chat_ChatRecord", new[] { "From_Id" });
            DropIndex("dbo.SignalR_Chat_ChatRecord", new[] { "Content_Id" });
            DropIndex("dbo.SignalR_User", new[] { "HeadImg_Id" });
            DropIndex("dbo.SignalR_Chat_ChatContent", new[] { "Voice_Id" });
            DropIndex("dbo.SignalR_Chat_ChatContent", new[] { "Picture_Id" });
            DropTable("dbo.UserChatGroups");
            DropTable("dbo.SignalR_Chat_ChatRecord");
            DropTable("dbo.SignalR_UserHeadImg");
            DropTable("dbo.SignalR_User");
            DropTable("dbo.SignalR_Chat_ChatGroup");
            DropTable("dbo.SignalR_Chat_ChatVoice");
            DropTable("dbo.SignalR_Chat_ChatPicture");
            DropTable("dbo.SignalR_Chat_ChatContent");
        }
    }
}
