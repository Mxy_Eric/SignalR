namespace SignalRApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changestructure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SignalR_Chat_ChatContent", "Picture_Id", "dbo.SignalR_Chat_ChatPicture");
            DropForeignKey("dbo.SignalR_Chat_ChatContent", "Voice_Id", "dbo.SignalR_Chat_ChatVoice");
            DropForeignKey("dbo.SignalR_User", "HeadImg_Id", "dbo.SignalR_UserHeadImg");
            DropForeignKey("dbo.SignalR_Chat_ChatRecord", "Content_Id", "dbo.SignalR_Chat_ChatContent");
            DropIndex("dbo.SignalR_Chat_ChatContent", new[] { "Picture_Id" });
            DropIndex("dbo.SignalR_Chat_ChatContent", new[] { "Voice_Id" });
            DropIndex("dbo.SignalR_User", new[] { "HeadImg_Id" });
            DropIndex("dbo.SignalR_Chat_ChatRecord", new[] { "Content_Id" });
            CreateTable(
                "dbo.SignalR_UserRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Role = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.SignalR_User", "PasswordHash", c => c.String(maxLength: 20));
            AddColumn("dbo.SignalR_User", "HeadImg", c => c.String());
            AddColumn("dbo.SignalR_User", "User_Id", c => c.Int());
            AddColumn("dbo.SignalR_User", "Role_Id", c => c.Int());
            AddColumn("dbo.SignalR_Chat_ChatRecord", "Content", c => c.String());
            AddColumn("dbo.SignalR_Chat_ChatRecord", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.SignalR_Chat_ChatRecord", "IsWithdrawed", c => c.Boolean(nullable: false));
            CreateIndex("dbo.SignalR_User", "User_Id");
            CreateIndex("dbo.SignalR_User", "Role_Id");
            AddForeignKey("dbo.SignalR_User", "User_Id", "dbo.SignalR_User", "Id");
            AddForeignKey("dbo.SignalR_User", "Role_Id", "dbo.SignalR_UserRole", "Id");
            DropColumn("dbo.SignalR_User", "Password");
            DropColumn("dbo.SignalR_User", "HeadImg_Id");
            DropColumn("dbo.SignalR_Chat_ChatRecord", "Content_Id");
            DropTable("dbo.SignalR_Chat_ChatContent");
            DropTable("dbo.SignalR_Chat_ChatPicture");
            DropTable("dbo.SignalR_Chat_ChatVoice");
            DropTable("dbo.SignalR_UserHeadImg");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SignalR_UserHeadImg",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        Suffix = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SignalR_Chat_ChatVoice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        CreateTime = c.String(),
                        UpdateTime = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SignalR_Chat_ChatPicture",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        Suffix = c.String(),
                        CreateTime = c.String(),
                        UpdateTime = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SignalR_Chat_ChatContent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        IsWithdrawed = c.Boolean(nullable: false),
                        Picture_Id = c.Int(),
                        Voice_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.SignalR_Chat_ChatRecord", "Content_Id", c => c.Int());
            AddColumn("dbo.SignalR_User", "HeadImg_Id", c => c.Int());
            AddColumn("dbo.SignalR_User", "Password", c => c.String(maxLength: 20));
            DropForeignKey("dbo.SignalR_User", "Role_Id", "dbo.SignalR_UserRole");
            DropForeignKey("dbo.SignalR_User", "User_Id", "dbo.SignalR_User");
            DropIndex("dbo.SignalR_User", new[] { "Role_Id" });
            DropIndex("dbo.SignalR_User", new[] { "User_Id" });
            DropColumn("dbo.SignalR_Chat_ChatRecord", "IsWithdrawed");
            DropColumn("dbo.SignalR_Chat_ChatRecord", "Type");
            DropColumn("dbo.SignalR_Chat_ChatRecord", "Content");
            DropColumn("dbo.SignalR_User", "Role_Id");
            DropColumn("dbo.SignalR_User", "User_Id");
            DropColumn("dbo.SignalR_User", "HeadImg");
            DropColumn("dbo.SignalR_User", "PasswordHash");
            DropTable("dbo.SignalR_UserRole");
            CreateIndex("dbo.SignalR_Chat_ChatRecord", "Content_Id");
            CreateIndex("dbo.SignalR_User", "HeadImg_Id");
            CreateIndex("dbo.SignalR_Chat_ChatContent", "Voice_Id");
            CreateIndex("dbo.SignalR_Chat_ChatContent", "Picture_Id");
            AddForeignKey("dbo.SignalR_Chat_ChatRecord", "Content_Id", "dbo.SignalR_Chat_ChatContent", "Id");
            AddForeignKey("dbo.SignalR_User", "HeadImg_Id", "dbo.SignalR_UserHeadImg", "Id");
            AddForeignKey("dbo.SignalR_Chat_ChatContent", "Voice_Id", "dbo.SignalR_Chat_ChatVoice", "Id");
            AddForeignKey("dbo.SignalR_Chat_ChatContent", "Picture_Id", "dbo.SignalR_Chat_ChatPicture", "Id");
        }
    }
}
