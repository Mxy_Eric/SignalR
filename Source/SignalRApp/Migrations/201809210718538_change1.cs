namespace SignalRApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SignalR_Chat_ChatContent", "IsWithdrawed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SignalR_Chat_ChatContent", "IsWithdrawed");
        }
    }
}
