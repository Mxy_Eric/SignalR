﻿using SignalRApp.Models.Dto;
using System.Collections.Generic;

namespace SignalRApp.ViewModel
{
    public class ChatListViewModel
    {
        public string GroupName { get; set; }
        public List<ChatRecordDto> Data { get; set; }
    }
}