﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalRApp.Common
{
    /// <summary>
    /// chartHub开放接口通用返回数据类型
    /// </summary>
    public class ChatHubResult
    {
        public bool ExecuteSuccess { get; set; }
        public object Data { get; set; }
        public string FailedReson { get; set; }

        public ChatHubResult()
        {
            ExecuteSuccess = true;
        }

        public ChatHubResult(string errorMsg)
        {
            FailedReson = errorMsg;
        }
    }
}