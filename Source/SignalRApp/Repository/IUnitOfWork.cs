﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SignalRApp.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        int Save();
        int ExecuteSqlCommand(string sql, params object[] parameters);
        IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters);
        IEnumerable SqlQuery(Type elementType, string sql, params object[] parameters);
    }
}