﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SignalRApp.Models
{
    [Table("SignalR_Chat_ChatGroup")]
    public class ChatGroup
    {
        public int Id { get; set; }
        [MaxLength(20)]
        public string Name { get; set; }
        public virtual ICollection<User> Users { get; set; }

        public ChatGroup()
        {
            this.Users = new HashSet<User>();
        }
    }
}