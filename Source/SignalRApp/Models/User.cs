﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SignalRApp.Models
{
    [Table("SignalR_User")]
    public class User : Entity.Entity
    {
        [MaxLength(15)]
        public string NickName { get; set; }
        [MaxLength(15)]
        public string UserName { get; set; }
        [MaxLength(15)]
        public string PhoneNum { get; set; }
        public string Email { get; set; }
        [MaxLength(20)]
        public string PasswordHash { get; set; }
        public string HeadImg { get; set; }
        public UserRole Role { get; set; }
        public virtual ICollection<ChatGroup> Groups { get; set; }
        public ICollection<User> Friends { get; set; }
    }
}