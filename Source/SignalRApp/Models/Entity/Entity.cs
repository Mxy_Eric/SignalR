﻿namespace SignalRApp.Models.Entity
{
    public class Entity
    {
        public int Id { get; set; }
        public string CreateTime { get; set; }
        public string UpdateTime { get; set; }
    }
}