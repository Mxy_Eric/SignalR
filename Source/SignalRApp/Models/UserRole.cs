﻿using SignalRApp.Contacts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SignalRApp.Models
{
    [Table("SignalR_UserRole")]
    public class UserRole : ISoftDelete
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public bool IsDeleted { get ; set; }
    }
}