﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalRApp.Models.Dto
{
    public class InsertChatRecordDto
    {
        public int Id { get; set; }
        public string CreateTime { get; set; }
        public int FromUserId { get; set; }
        public int ToGroupId { get; set; }
        public int ToUserId { get; set; }
        public string Content { get; set; }
        public int Type { get; set; }
    }
}