﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalRApp.Models.Dto
{
    public class ChatRecordDto
    {
        public int Id { get; set; }
        public string CreateTime { get; set; }
        public User From { get; set; }
        public ChatGroup ToGroup { get; set; }
        public User ToUser { get; set; }
        public string Content { get; set; }
        public int Type { get; set; }
        public bool IsWithdrawed { get; set; }
    }
}