﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalRApp.Models.Dto
{
    /// <summary>
    /// 发送信息后的响应体
    /// </summary>
    public class SendMsgResultDto
    {
        /// <summary>
        /// 成功不关注msg,失败要提示
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 发消息操作是否成功
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 默认结果是成功的
        /// </summary>
        public SendMsgResultDto()
        {
            IsSuccess = true;
        }
        /// <summary>
        /// 发送失败就用这个
        /// </summary>
        /// <param name="msg"></param>
        public SendMsgResultDto(string msg)
        {
            IsSuccess = false;
            Message = msg;
        }
    }
}