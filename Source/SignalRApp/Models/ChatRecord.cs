﻿using SignalRApp.Contacts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SignalRApp.Models
{
    [Table("SignalR_Chat_ChatRecord")]
    public class ChatRecord: Entity.Entity,ISoftDelete
    {
        public string Content { get; set; }
        public int Type { get; set; }
        public virtual User From { get; set; }
        public virtual ChatGroup ToGroup { get; set; }
        public virtual User ToUser { get; set; }
        public bool IsWithdrawed { get; set; }
        public bool IsDeleted { get; set; }
    }
} 