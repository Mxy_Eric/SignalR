﻿using SignalRApp.Autofac;
using SignalRApp.Models;
using SignalRApp.Repository;

namespace SignalRApp.Service
{
    public interface IChatGroupService: IDependency
    {
        ChatGroup GetById(int id);
    }

    public class ChatGroupService : IChatGroupService
    {
        private readonly Repository<ChatGroup> repository;

        public ChatGroupService(Repository<ChatGroup> repository)
        {
            this.repository = repository;
        }

        public ChatGroup GetById(int id)
        {
            return repository.GetById(id);
        }
    }
}