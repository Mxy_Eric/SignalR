﻿using SignalRApp.Autofac;
using SignalRApp.Models;
using SignalRApp.Repository;

namespace SignalRApp.Service
{
    public interface IUserService: IDependency
    {
        User GetUserById(int id);
    }

    public class UserService : IUserService
    {
        private readonly Repository<User> repository;

        public UserService(Repository<User> repository)
        {
            this.repository = repository;
        }

        public User GetUserById(int id)
        {
            var user = repository.GetById(id);
            return user;
        }
    }
}