﻿using SignalRApp.Autofac;
using SignalRApp.Models;
using SignalRApp.Repository;
using System.Collections.Generic;
using System.Linq;

namespace SignalRApp.Service
{
    public interface IChatRecordService: IDependency
    {
        List<ChatRecord> GetListByGroupId(int groupId);
        int InsertRecord(ChatRecord chat);
        ChatRecord GetRecordById(int id);
        int UpdateRecord(ChatRecord chat);
    }

    public class ChatRecordService:IChatRecordService
    {
        private readonly Repository<ChatRecord> _repository;

        public ChatRecordService(Repository<ChatRecord> repository)
        {
            this._repository = repository;
        }

        public List<ChatRecord> GetListByGroupId(int groupId)
        {
            var chatRecords = _repository.Get(x => x.ToGroup.Id == groupId);
            
            return chatRecords.ToList();
        }

        public ChatRecord GetRecordById(int id)
        {
            var record = _repository.GetById(id);
            return record;
        }

        public int InsertRecord(ChatRecord chat)
        {
            if (chat==null)
            {
                return 0;
            }
            _repository.Insert(chat);
            return _repository.UnitOfWork.Save();
        }

        public int UpdateRecord(ChatRecord chat)
        {
            _repository.Update(chat);
            return _repository.UnitOfWork.Save();
        }
    }
}