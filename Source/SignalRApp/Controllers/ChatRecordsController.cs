﻿using Microsoft.AspNet.SignalR;
using SignalRApp.Models;
using SignalRApp.Models.Dto;
using SignalRApp.Service;
using SignalRApp.SignalR;
using SignalRApp.ViewModel;
using System;
using System.Linq;
using System.Web.Http;

namespace SignalRApp.Controllers
{
    public class ChatRecordsController : ApiController
    {
        private readonly IChatRecordService _chatRecordService;
        private readonly IChatGroupService _chatGroupService;
        private readonly IUserService _userService;
        public ChatRecordsController(IChatRecordService chatRecordService,
            IChatGroupService chatGroupService,
            IUserService userService)
        {
            _chatRecordService = chatRecordService;
            _chatGroupService = chatGroupService;
            _userService = userService;
        }

        // GET: api/ChatRecords
        public IHttpActionResult GetChatRecords(int id)
        {
            string groupName = string.Empty;
            var group = _chatGroupService.GetById(id);
            groupName = group?.Name;
            var chatRecord = _chatRecordService.GetListByGroupId(id);
            var chatRecordDtos = (from s in chatRecord
                                  select new ChatRecordDto
                                  {
                                      Id = s.Id,
                                      Content = s.Content,
                                      CreateTime = s.CreateTime,
                                      From = s.From,
                                      ToGroup = s.ToGroup,
                                      ToUser = s.ToUser
                                  }).ToList();
            ChatListViewModel result = new ChatListViewModel
            {
                GroupName = groupName,
                Data = chatRecordDtos
            };

            return Ok(result);
        }


        public IHttpActionResult T(byte[] test)
        {
            

            return Ok(test);
        }

        [HttpPost]
        public IHttpActionResult SendMsg([FromBody]InsertChatRecordDto chatDto)
        {
            if (chatDto == null)
            {
                return BadRequest();
            }
            chatDto.CreateTime = DateTime.Now.ToString("yyyy年MM月dd日 HH:mm:ss");
            var ToUser = _userService.GetUserById(chatDto.ToUserId);
            var FromUser = _userService.GetUserById(chatDto.FromUserId);
            var ToGroup = _chatGroupService.GetById(chatDto.ToGroupId);
            if (FromUser == null)
            {
                return BadRequest("你还没有加入群聊");
            }
            ChatRecord record = new ChatRecord
            {
                Content = chatDto.Content,
                CreateTime = chatDto.CreateTime,
                From = FromUser,
                Id = chatDto.Id,
                ToGroup = ToGroup,
                ToUser = ToUser
            };
            var result= _chatRecordService.InsertRecord(record);
            if (result > 0)
            {
                GlobalHost.ConnectionManager.GetHubContext<ChatHub>().Clients.All.receive(chatDto);
                return Ok("Success");
            }
            return Ok("Failed");
        }

        [HttpGet]
        public IHttpActionResult WithdrawContent(int recordId)
        {
            if (recordId == null)
            {
                return BadRequest("Empty param");
            }
            var record = _chatRecordService.GetRecordById(recordId);
            if (record == null)
            {
                return BadRequest("Not exsit data");
            }
            record.IsWithdrawed = true;
            var result = _chatRecordService.UpdateRecord(record);
            if (result > 0)
            {
                GlobalHost.ConnectionManager.GetHubContext<ChatHub>().Clients.All.receive("");
                return Ok("success");
            }
            else
            {
                return Ok("failed");
            }
        }
    }
}